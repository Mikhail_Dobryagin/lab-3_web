function clickGraph(event) {
    let r = Number($('#input_form\\:r_duplicate').text());

    if (r === undefined) {
        wrongInputAnimation($('#r_label'));
        return;
    }

    let xPage = event.pageX - $('#graph').offset().left;
    let x = ((xPage - 110) * r / (176 - 110)).toFixed(2);

    let yPage = event.pageY - $('#graph').offset().top;
    let y = ((110 - yPage) * r / (176 - 110)).toFixed(2);

    flag = false;

    if (!checkX(x)) {
        console.log(x)
        wrongInputAnimation($('#x_label'));
        flag = true;
    }

    if (!checkY(y)) {
        console.log(y)
        wrongInputAnimation($('#y_label'));
        flag = true;
    }

    if(flag)
        return;
    // drawPoint(xPage, yPage);

    $('#input_form\\:hidden_x').val(x);
    $('#input_form\\:hidden_y').val(y);
    $('#input_form\\:hidden_x').trigger('change');
    $('#input_form\\:hidden_x').trigger('change');
    $('#input_form\\:hidden_y').trigger('change');
    $('#input_form\\:hidden_y').trigger('change');
    $('#input_form\\:y_text').val("0.0");
    $('#input_form\\:submit_button_real').trigger('click');
}

function drawPoint(xPage, yPage, result) {

    $('#graph circle:last-child').attr("cx", xPage);
    $('#graph circle:last-child').attr("cy", yPage);
    $('#graph circle:last-child').attr("r", 2);

    if(result === undefined)
        $('#graph circle:last-child').attr("fill", "#003850");
    else
    {
        $('#graph circle:last-child').attr("fill", result === false ? "#ff5454" : "#08E72AFF");
        console.log(result);
    }

}

function redrawPoints() {
    if (r_old === undefined)
        return;

    $('.point').each(function () {

        if (this.getAttribute("cx") === undefined) {
            alert("Это последний элемент");
            return;
        }

        let xPage = this.getAttribute("cx");
        let x = (xPage - 110) * r_old / (176 - 110);

        let yPage = this.getAttribute("cy");
        let y = (110 - yPage) * r_old / (176 - 110);


        xPage = x / r * (176 - 110) + 110;
        yPage = 110 - y / r * (176 - 110);

        this.setAttribute("cx", xPage);
        this.setAttribute("cy", yPage);
    });
}