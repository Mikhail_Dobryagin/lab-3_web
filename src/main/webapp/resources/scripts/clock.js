$(document).ready(function () {
    runClock();
});

async function runClock() {
    let curDate = new Date(Date.now());
    $('#clock').text((curDate.getHours() < 10 ? "0" : "") + curDate.getHours() + ":" + (curDate.getMinutes()<10 ? "0" : "") + curDate.getMinutes());

    setInterval(function () {
        curDate = new Date(Date.now());
        $('#clock').text((curDate.getHours() < 10 ? "0" : "") + curDate.getHours() + ":" + (curDate.getMinutes()<10 ? "0" : "") + curDate.getMinutes());
    }, 11000);
}

