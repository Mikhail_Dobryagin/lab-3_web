$(document).ready(function () {

    $('#input_form').on('submit', submitForm);

    $('#reset_button').on('click', function () {
        $('#input_form\\:reset_button_real').trigger('click');
        $('#input_form\\:y_text').val(0);
        $('#input_form\\:y_text').text(0);
    });
});

function toIndexPage() {
    $('#return_form\\:return_button').trigger('click');
}

function submitForm() {
    flag = true;
    let r = Number($('#input_form\\:r_duplicate').text());
    let x = Number($('#input_form\\:x_duplicate').text());
    let y = Number($('#input_form\\:y_text').val());

    if (isNaN(r) || !(2 <= r && r <= 5)) {
        wrongInputAnimation($('#r_label'));
        flag = false;
    }


    if (!checkX(x)) {
        wrongInputAnimation($('#x_label'));
        flag = false;
    }

    if (!checkY(y)) {
        console.log(y)
        wrongInputAnimation($('#y_label'));
        flag = false;
    }

    if (flag)
        $('#input_form\\:submit_button_real').trigger('click');

}


async function wrongInputAnimation(element) {
    element.animate({
        opacity: 0
    }, 500);

    setTimeout(function () {
        element.css("color", "red");

        element.animate({
            opacity: 1
        }, 500);

        setTimeout(function () {

            element.animate({
                opacity: 0
            }, 2500);

            setTimeout(function () {
                element.css("color", "black");

                element.animate({
                    opacity: 1
                }, 500);
            }, 2500);

        }, 500);
    }, 500);
}

function checkX(x) {
    return !isNaN(x) && -5 <= x && x <= 5;
}

function checkY(y) {
    return !isNaN(y) && -3 <= y && y <= 5;
}