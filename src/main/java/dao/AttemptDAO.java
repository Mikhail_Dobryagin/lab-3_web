package dao;

import beans.Attempt;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@ManagedBean(name = "attemptDAO", eager = true)
@SessionScoped
public class AttemptDAO
{

    public void add(Attempt bean)
    {
        if (bean.validate()) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.save(bean);
            tx.commit();
            session.close();
        }
        bean.setDefault();
    }

    public List<Attempt> getAll()
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        List list = session.createQuery("FROM Attempt " + "WHERE sId=" + FacesContext.getCurrentInstance().getExternalContext().getSessionId(false).hashCode() + " order by id desc").getResultList();
        tx.commit();
        session.close();
        return list;
    }

    public void delete()
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.createQuery("DELETE FROM Attempt " + "WHERE sId=" + FacesContext.getCurrentInstance().getExternalContext().getSessionId(true).hashCode()).executeUpdate();
        tx.commit();
        session.close();
    }
}
