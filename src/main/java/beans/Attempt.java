package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ManagedBean(name = "attemptBean")
@SessionScoped
public class Attempt implements Serializable
{
    private double x, y, r;

    private String curTime;
    private double execTime;
    private int result;
    private Long id;

    private Integer sId;

    public Integer getsId()
    {
        return sId;
    }

    public void setsId(Integer sId)
    {
        this.sId = sId;
    }

    public Attempt()
    {
        setDefault();
        r = 3.5;
    }

    public Attempt(double x, double y, double r)
    {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public double getX()
    {
        return x;
    }

    public void setX(double x)
    {
        if (-5 <= x && x <= 5)
            this.x = x;
    }

    public double getY()
    {
        return y;
    }

    public void setY(double y)
    {
        if (-3 <= y && y <= 5)
            this.y = y;
    }

    public double getR()
    {
        return r;
    }

    public void setR(double r)
    {

        if (2 <= r && r <= 5)
            this.r = r;
    }

    public String getCurTime()
    {
        return curTime;
    }

    public void setCurTime(String curTime)
    {
        this.curTime = curTime;
    }

    public double getExecTime()
    {
        return execTime;
    }

    public void setExecTime(double execTime)
    {
        this.execTime = execTime;
    }

    public int getResult()
    {
        return result;
    }

    public void setResult(int result)
    {
        this.result = result;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public Attempt check()
    {
        this.sId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(false).hashCode();
        long startTime = System.nanoTime();
        curTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));

        result = check1() || check2() || check3() || check4() ? 1 : 0;

        execTime = System.nanoTime() - startTime;

        return this;
    }

    private boolean check1()
    {
        return false;
    }

    private boolean check2()
    {
        //y = -2x + R
        return x <= 0 && 0 <= y && y <= 2. * x + r;
    }

    private boolean check3()
    {
        return y <= 0 && x <= 0 && y * y + x * x <= r * r;
    }

    private boolean check4()
    {
        return 0 <= x && x <= r && -r / 2. <= y && y <= 0;
    }

    public void xSliderChanged(ValueChangeEvent event)
    {
        x = ((Integer) event.getNewValue()).doubleValue();
    }

    public void rSliderChanged(ValueChangeEvent event)
    {
        r = 2. + ((Integer) event.getNewValue()).doubleValue() / 4.;
    }

    public void yChanged(ValueChangeEvent event)
    {
        y = Double.parseDouble(event.getNewValue().toString());
    }

    public void xChanged(ValueChangeEvent event)
    {

    }

    public void change(AjaxBehaviorEvent event)
    {
        if (event.getComponent().getId().equals("hidden_x"))
            x = Double.parseDouble(event.getComponent().getAttributes().get("value").toString());
        else
            y = Double.parseDouble(event.getComponent().getAttributes().get("value").toString());
    }

    public void setDefault()
    {
        x = 0;
        y = 0;
        execTime = 0;
        result = 0;
    }

    public boolean validate()
    {
        return (-5. <= x && x <= 5.) && (-3. <= y && y <= 5) && (2 <= r && r <= 5);
    }
}
